import { TestBed, async } from "@angular/core/testing";
import { BrowserModule } from "@angular/platform-browser";
import { IonRangeSliderModule } from "ng-lib";

import { AppComponent } from "./app.component";


describe("AppComponent", () =>
{
	beforeEach(async(() =>
	{
		TestBed.configureTestingModule({
			imports: [
				BrowserModule,
				IonRangeSliderModule
			],
			declarations: [
				AppComponent
			],
		}).compileComponents();
	}));

	it("should create the app", async(() =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));

	it("should have as title 'ng-ion-range-slider'", async(() =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app.title).toEqual("ng-ion-range-slider");
	}));

	it("should render title in a H1 tag", (() =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector("h1").textContent).toContain("ng-ion-range-slider example");
	}));

});
