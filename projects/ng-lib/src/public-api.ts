/*
 * Public API Surface of ng-lib.
 */

export * from "./lib/ion-range-slider.module";
export * from "./lib/ion-range-slider.component";
